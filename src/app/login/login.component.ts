import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { AppService } from '../app.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  pseudo: string;
  password: string;

  constructor(private router: Router,private http:HttpClient,private cookieService: CookieService,private appService : AppService ) {
  }


  postData() {

    this.http
      .post("http://localhost:3000/users/login", {pseudo: this.pseudo,password: this.password},{ responseType: 'text' })
      .subscribe(res => {
        console.log(res)
        var data = JSON.parse(res)
        if(data.token){
          this.cookieService.set( 'Token', data.token );
          this.appService.checkUser()
          this.router.navigate(['/profile']);
        }else{
          alert('Pseudo or password incorrect')
        }

      });
  }

  onClick() {
    this.postData()

  }


  ngOnInit() {
  }

}
