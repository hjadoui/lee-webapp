import { Component, OnInit } from '@angular/core';
import { HttpClient,HttpParams,HttpHeaders } from '@angular/common/http';
import {Router} from "@angular/router";




@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  pseudo: string;
  email: string;
  password: string;
  success = false;



  constructor(private router: Router,private http:HttpClient) {
  }


  postData() {
    this.http
      .post("http://localhost:3000/users", {pseudo: this.pseudo,email: this.email,password: this.password},{ responseType: 'text' })
      .subscribe(res => {
        var data = JSON.parse(res)
        if (data.success){
          this.success = true
        }
      });
  }

  onClick() {
    this.postData()
  }

  ngOnInit() {
  }
}
