import { Injectable , EventEmitter, Output} from '@angular/core';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  user : boolean;

  @Output() change: EventEmitter<boolean> = new EventEmitter();

  constructor(private cookieService : CookieService) { }

  checkUser(){
    if(this.cookieService.get('Token')) {
      this.user = true;
    }else{
      this.user = false;
    }

    this.change.emit(this.user)

  }
}
