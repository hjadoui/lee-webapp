import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  pseudo: string;
  email : string;
  articles : Object = []

  constructor(private http:HttpClient,private cookieService: CookieService ,private router: Router) {
  }

  ngOnInit() {

      if(this.cookieService.get('Token')){
        const httpOptions = {
          headers: new HttpHeaders({
            'Authorization': this.cookieService.get('Token')
          }),
          responseType: 'text' as 'text'
        };
        this.http
          .get("http://localhost:3000/users/profile",httpOptions)
          .subscribe(res => {
            var data = JSON.parse(res)
            if(data.pseudo && data.email) {
              this.pseudo = data.pseudo;
              this.email = data.email;
            }
            else
              console.log(data)

            if(data.articles){
              this.articles = data.articles;
            }
            console.log(data.articles)
          });
      }

      else{
        this.router.navigate(['']);
      }
    }



}
