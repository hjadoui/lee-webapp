import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {SignupComponent} from './signup/signup.component';
import {HomeComponent} from './home/home.component';
import {LoginComponent} from './login/login.component';
import {NewarticleComponent} from './newarticle/newarticle.component';
import {ProfileComponent} from './profile/profile.component';
import {ArticleComponent} from './article/article.component';


const routes: Routes = [
  {path:'signup',component:SignupComponent},
  {path:'login',component:LoginComponent},
  {path:'profile',component:ProfileComponent},
  {path:'newarticle',component:NewarticleComponent},
  {path:'article',component:ArticleComponent},
  {path:'',component:HomeComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
