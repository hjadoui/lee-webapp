import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-newarticle',
  templateUrl: './newarticle.component.html',
  styleUrls: ['./newarticle.component.css']
})
export class NewarticleComponent implements OnInit {

  title: string;
  article: string;
  category: string;
  success = false ;

  constructor(private http:HttpClient,private cookieService: CookieService) {
  }


  postData() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Authorization': this.cookieService.get('Token')
      })
    }
    this.http
      .post("http://localhost:3000/articles", {title: this.title,article: this.article,category: this.category},httpOptions)
      .subscribe(res => {
        this.success = true;
      });
  }

  onClick() {
    this.postData()
  }

  ngOnInit() {
  }

}
