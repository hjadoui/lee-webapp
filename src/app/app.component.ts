import {Component, OnInit} from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import {HttpClient} from "@angular/common/http";
import { AppService } from './app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'webapp';
  user : boolean;

  constructor( private cookieService: CookieService, private appService:AppService ,private router: Router) {
  }

  ngOnInit() {
    if(this.cookieService.get('Token')) {
      this.user = true;
    }else{
      this.user = false;
    }
    this.appService.change.subscribe(user => {
      this.user = user;
    });
  }

  onClickLogOut(){
    this.cookieService.delete('Token');
    this.user = false;
    this.router.navigate(['']);
  }

}
