import { Component, OnInit } from '@angular/core';
import { HttpClient,HttpParams,HttpHeaders } from '@angular/common/http';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  articles : Object = []

  search : string;

  constructor(private http:HttpClient) { }

  getData() {
    this.http.get('http://localhost:3000/articles')
      .subscribe(res => {
        this.articles = res;
        console.log(res);
      });
  }

  onClickSearch(){
    this.http.get('http://localhost:3000/articles',{params:{search : this.search}})
      .subscribe(res => {
        this.articles = res;
        console.log(res);
      });
  }

  ngOnInit() {
    this.getData()
  }

}
