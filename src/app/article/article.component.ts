import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {HttpClient} from "@angular/common/http";
@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css']
})
export class ArticleComponent implements OnInit {

  title: string;
  content : string;
  category: string;
  user : string;

  constructor(private router: ActivatedRoute,private http:HttpClient) { }

  ngOnInit() {

    var url = 'http://localhost:3000/users/'+this.router.snapshot.queryParamMap.get('author');

    this.http.get(url,{responseType:'text'})
      .subscribe(res => {
        var data = JSON.parse(res)
        this.user = data.pseudo
      });

    this.title = this.router.snapshot.queryParamMap.get('title')
    this.content = this.router.snapshot.queryParamMap.get('content')
    this.category = this.router.snapshot.queryParamMap.get('category')



  }

}
